/***********************************************
   Plasma Robotics Microbot

   Starter Sketch

   https://gitlab.com/plasmarobotics/plasma-microbot/

   Install the ESP32 Board Into Arduino IDE

   https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/

   These Libraries need to be installed
       Dual Motor Driver - TB6612FNG_ESP32
        https://github.com/pablopeza/TB6612FNG_ESP32
       PS3 - esp32-ps3
        https://github.com/jvpernis/esp32-ps3

   Select the "ESP32 Dev Module" Board

 ************************************************/

// *** PS3 Controller ************************************
#include <Ps3Controller.h>
int playerLED = 1;

// *** Motor Driver TB6612 *******************************
#include <TB6612_ESP32.h>

#define leftAIN1 25
#define leftBIN1 26
#define leftAIN2 33
#define leftBIN2 27
#define leftPWMA 32
#define leftPWMB 14

#define rightAIN1 5
#define rightBIN1 17
#define rightAIN2 18
#define rightBIN2 16
#define rightPWMA 19
#define rightPWMB 4

//Not using standby
#define STBY 23

// these constants are used to allow you to make your motor configuration
// line up with function names like forward.  Value can be 1 or -1
const int leftOffsetA = 1;
const int leftOffsetB = 1;
const int rightOffsetA = -1;
const int rightOffsetB = -1;

// Initializing motors.  The library will allow you to initialize as many
// motors as you have memory for.  If you are using functions like forward
// that take 2 motors as arguements you can either write new functions or
// call the function more than once.
Motor leftMotor1  = Motor(leftAIN1, leftAIN2, leftPWMA, leftOffsetA, STBY, 5000 , 8, 1 );
Motor leftMotor2  = Motor(leftBIN1, leftBIN2, leftPWMB, leftOffsetB, STBY, 5000 , 8, 2 );
Motor rightMotor1 = Motor(rightAIN1, rightAIN2, rightPWMA, rightOffsetA, STBY, 5000 , 8, 3 );
Motor rightMotor2 = Motor(rightBIN1, rightBIN2, rightPWMB, rightOffsetB, STBY, 5000 , 8, 4 );

int lMotorSpeed = 0;
int rMotorSpeed = 0;


void setup() {
  Serial.begin(115200);
  while (!Serial) {
    delay(10); // Wait until serial console opens
  }

  // *** PS3 Controller ************************************
  // Change MAC to match the PS3 controller in you kit
  Ps3.begin("00:00:00:02:07:11");
}

void getPS3() {
  while (true) {
    if (!Ps3.isConnected())
      return;
    //------ Digital Up/Down/Left/Right buttons ------
    if ( Ps3.data.button.down ) {
      leftMotor2.drive(-150);
      rightMotor2.drive(-150);
    }
    if ( Ps3.data.button.left ) {
      leftMotor2.drive(-150);
      rightMotor2.drive(150);
    }
    if ( Ps3.data.button.up ) {
      leftMotor2.drive(250);
      rightMotor2.drive(250);
    }
    if (Ps3.data.button.right ) {
      leftMotor2.drive(150);
      rightMotor2.drive(-150);
    }
    delay(10);
    brake(leftMotor1, leftMotor2);
    brake(rightMotor1, rightMotor2);
    
    //---------------- Analog stick value events ---------------

    if ( abs(Ps3.data.analog.stick.rx) > 2 || abs(Ps3.data.analog.stick.ly) > 2 ) {
      lMotorSpeed = ((Ps3.data.analog.stick.ly * -3 + Ps3.data.analog.stick.rx * 2 ) / sqrt(2));
      rMotorSpeed = ((Ps3.data.analog.stick.ly * -3 - Ps3.data.analog.stick.rx * 2) / sqrt(2));

      if (lMotorSpeed > 255)lMotorSpeed = 255;
      if (lMotorSpeed < -255)lMotorSpeed = -255;
      if (rMotorSpeed > 255)rMotorSpeed = 255;
      if (rMotorSpeed < -255)rMotorSpeed = -255;
      leftMotor2.drive(lMotorSpeed);
      rightMotor2.drive(rMotorSpeed);  
    }
    
    delay(10);
    brake(leftMotor1, leftMotor2);
    brake(rightMotor1, rightMotor2);

    //---------- Digital select/start button events ---------
    if ( Ps3.event.button_down.select )
      Ps3.setPlayer(2);
    if ( Ps3.event.button_down.start )
      Ps3.setPlayer(4);


  }
}


void loop() {

  if (!Ps3.isConnected())
    return;

  //-------------------- Player LEDs -------------------
  Serial.println("Setting LEDs to player ");
  Ps3.setPlayer(2);


  getPS3();

}
