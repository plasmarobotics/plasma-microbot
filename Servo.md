# Servo 

Here you can find a How To using a Servo with an ESP32 - [randomnerdtutorials](https://randomnerdtutorials.com/esp32-servo-motor-web-server-arduino-ide/)

![Servo Pic](https://gitlab.com/plasmarobotics/plasma-microbot/-/raw/main/img/servo.png)

## Library

[ServoESP32](https://github.com/RoboticsBrno/ServoESP32)

## Pinout

| Wire | Color | Notes |
| ------ | ------ | ------ |
| GND | Brown | Connect to Ground |
| Power 5v | Red | Connect to the 5v Power rail |
| Signal | Orange | Connect to a GPIO Port that supports PWM |

## Code Example


```
#include <Servo.h>

static const int servoPin = 12;

Servo servo1;

void setup() {
    Serial.begin(115200);
    servo1.attach(servoPin);
}

void loop() {
    for(int posDegrees = 0; posDegrees <= 180; posDegrees++) {
        servo1.write(posDegrees);
        Serial.println(posDegrees);
        delay(20);
    }

    for(int posDegrees = 180; posDegrees >= 0; posDegrees--) {
        servo1.write(posDegrees);
        Serial.println(posDegrees);
        delay(20);
    }
}

```

---
Back to main page [Home](https://gitlab.com/plasmarobotics/plasma-microbot)
