/***********************************************
   Plasma Robotics Microbot

   Starter Sketch

   https://gitlab.com/plasmarobotics/plasma-microbot/

   Install the ESP32 Board Into Arduino IDE

   https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/

   These Libraries need to be installed
     * Dual Motor Driver - TB6612FNG_ESP32
        https://github.com/pablopeza/TB6612FNG_ESP32
     * Accelerometer and Gyro - Adafruit_MPU6050
        https://github.com/adafruit/Adafruit_MPU6050
     * PS3 - esp32-ps3
        https://github.com/jvpernis/esp32-ps3

   Select the "ESP32 Dev Module" Board

 ************************************************/

// *** Accelerometer/Gyroscope Adafruit MPU6050 **********
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>

Adafruit_MPU6050 mpu;

// *** Motor Driver TB6612 *******************************
#include <TB6612_ESP32.h>

// Pins for all inputs, keep in mind the PWM defines must be on PWM pins
// the default pins listed are the ones used on the Redbot (ROB-12097) with
// the exception of STBY which the Redbot controls with a physical switch
#define leftAIN1 25
#define leftBIN1 26
#define leftAIN2 33
#define leftBIN2 27
#define leftPWMA 32
#define leftPWMB 14

#define rightAIN1 5
#define rightBIN1 17
#define rightAIN2 18
#define rightBIN2 16
#define rightPWMA 19
#define rightPWMB 4

//Not using standby
#define STBY 23

// these constants are used to allow you to make your motor configuration
// line up with function names like forward.  Value can be 1 or -1
const int leftOffsetA = 1;
const int leftOffsetB = 1;
const int rightOffsetA = 1;
const int rightOffsetB = 1;

// Initializing motors.  The library will allow you to initialize as many
// motors as you have memory for.  If you are using functions like forward
// that take 2 motors as arguements you can either write new functions or
// call the function more than once.
Motor leftMotor1  = Motor(leftAIN1, leftAIN2, leftPWMA, leftOffsetA, STBY, 5000 , 8, 1 );
Motor leftMotor2  = Motor(leftBIN1, leftBIN2, leftPWMB, leftOffsetB, STBY, 5000 , 8, 2 );
Motor rightMotor1 = Motor(rightAIN1, rightAIN2, rightPWMA, rightOffsetA, STBY, 5000 , 8, 3 );
Motor rightMotor2 = Motor(rightBIN1, rightBIN2, rightPWMB, rightOffsetB, STBY, 5000 , 8, 4 );


// *** PS3 Controller ************************************
#include <Ps3Controller.h>
int playerLED = 1;


void setup() {
  Serial.begin(115200);
  while (!Serial) {
    delay(10); // Wait until serial console opens
  }

  // *** Accelerometer/Gyroscope Adafruit MPU6050 **********
  // Try to initialize!
  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {
      delay(10);
    }
  }

  mpu.setAccelerometerRange(MPU6050_RANGE_16_G);
  mpu.setGyroRange(MPU6050_RANGE_250_DEG);
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  Serial.println("");
  delay(100);


  // *** PS3 Controller ************************************
  Ps3.attach(notify);
  Ps3.attachOnConnect(onConnect);
  // Change MAC to match the PS3 controller in you kit
  Ps3.begin("01:01:01:01:01:01");
  Ps3.setPlayer(playerLED);


}

void getMPU6050() {
  // *** Accelerometer/Gyroscope Adafruit MPU6050 **********

  /* Get new sensor events with the readings */
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);

  /* Print out the values */
  Serial.print(a.acceleration.x);
  Serial.print(",");
  Serial.print(a.acceleration.y);
  Serial.print(",");
  Serial.print(a.acceleration.z);
  Serial.print(", ");
  Serial.print(g.gyro.x);
  Serial.print(",");
  Serial.print(g.gyro.y);
  Serial.print(",");
  Serial.print(g.gyro.z);
  Serial.println("");
}

void getTB6612() {
  //Use of the drive function which takes as arguements the speed
  //and optional duration.  A negative speed will cause it to go
  //backwards.  Speed can be from -255 to 255.  Also use of the
  //brake function which takes no arguements.
  leftMotor1.drive(255, 1000);
  leftMotor1.drive(-255, 1000);
  leftMotor1.brake();
  delay(100);
  leftMotor2.drive(255, 1000);
  leftMotor2.drive(-255, 1000);
  leftMotor2.brake();
  delay(100);
  rightMotor1.drive(255, 1000);
  rightMotor1.drive(-255, 1000);
  rightMotor1.brake();
  delay(100);
  rightMotor2.drive(255, 1000);
  rightMotor2.drive(-255, 1000);
  rightMotor2.brake();
  delay(100);
  //Use of the forward function, which takes as arguements two motors
  //and optionally a speed.  If a negative number is used for speed
  //it will go backwards
  forward(leftMotor1, rightMotor1, 150);
  delay(100);
  //Use of the back function, which takes as arguments two motors
  //and optionally a speed.  Either a positive number or a negative
  //number for speed will cause it to go backwards
  back(leftMotor2, rightMotor2, -150);
  delay(100);
  brake(leftMotor1, leftMotor2);
  brake(rightMotor1, rightMotor2);
  delay(100);

}

void getPS3() {
  while (true) {
    if (!Ps3.isConnected())
      return;
    //------ Digital Up/Down/Left/Right buttons ------
    if ( Ps3.data.button.down ) {
      Serial.println("Pressing cross buttons");
      back(leftMotor1, leftMotor2, -150);
      back(rightMotor1, rightMotor2, -150);
    }
    if ( Ps3.data.button.left ) {
      Serial.println("Pressing left buttons");
      back(leftMotor1, leftMotor2, 150);
      forward(rightMotor1, rightMotor2, 150);
    }
    if ( Ps3.data.button.up ) {
      Serial.println("Pressing up buttons");
      forward(leftMotor1, leftMotor2, 150);
      forward(rightMotor1, rightMotor2, 150);
    }
    if (Ps3.data.button.right ) {
      Serial.println("Pressing right buttons");
      forward(leftMotor1, leftMotor2, 150);
      back(rightMotor1, rightMotor2, 150);
    }

  delay(10);
  brake(leftMotor1, leftMotor2);
  brake(rightMotor1, rightMotor2);
  
  }
}

// Functions for PS3 if needed
void notify(){
  Serial.println("Notify.");
}

void onConnect() {
  Serial.println("Connected.");
}

void loop() {
  // Sample code

  // *** Accelerometer/Gyroscope Adafruit MPU6050 **********
  /* Get new sensor events with the readings */
  getMPU6050();

  // *** Motor Driver TB6612 *******************************
  /* Demo Motor Test */
  getTB6612();

  // *** PS3 Controller ************************************
  /* Demo Motor Test */
  getPS3();

}
