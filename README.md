# Plasma Microbot

DIY microbot kit to teach robotics and have fun.

Download this example to start - [Download](https://gitlab.com/plasmarobotics/plasma-microbot/-/raw/main/plasmaMicrobotExample/plasmaMicrobotExample.ino?inline=false)

# Components

## PCB Main Board

Main Board - [EasyEDA](https://oshwlab.com/erik.pope/plasma-robot-kit)

![PCB](https://gitlab.com/plasmarobotics/plasma-microbot/-/raw/main/img/microbot-pcb.PNG)

Board is 100mm x 100mm

Holes are 88mm x 88mm

## Micro Controller

ESP32-DevKitC V4 - ESP32-WROOM-32D - [Site](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/hw-reference/esp32/get-started-devkitc.html)

![ESP32-DevKitC V4 Pinout](https://raw.githubusercontent.com/espressif/esp-idf/98ad01e5fc0779d11209350d8ec31fe3d2137ed5/docs/_static/esp32-devkitC-v4-pinout.jpg)

Add micro Controller to Arduino IDE -
https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/

## Motor Driver

TB6612FNG - [Site](https://github.com/pablopeza/TB6612FNG_ESP32)

![TB6612FNG](https://camo.githubusercontent.com/455800601bf73562da2cf2a4efc77ca97f111cae742bae55e392965994ed1994/68747470733a2f2f63646e2e737061726b66756e2e636f6d2f2f6173736574732f70617274732f332f312f352f372f30393435372d3031622e6a7067)

Other Info - [Site](https://nkmakes.github.io/2020/09/02/esp32-tank-robot-joystick-http-web-control/)

## Accelerometer/Gyroscope

MPU-6050 - [randomnerdtutorials](https://randomnerdtutorials.com/esp32-mpu-6050-accelerometer-gyroscope-arduino/) - [adafruit](https://github.com/adafruit/Adafruit_MPU6050)

![MPU-6050](https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2020/12/MPU6050-Module-Accelerometer-Gyroscope-Temperature-Sensor.jpg?w=750&quality=100&strip=all&ssl=1)

Example - 
https://github.com/adafruit/Adafruit_MPU6050/blob/master/examples/plotter/plotter.ino

## Game controller

Info on how to use with ESP32 - [anyonecanbuildrobots](https://www.anyonecanbuildrobots.com/post/esp32-ps3-controller-robotic-arm-awesome)

![Game controller](https://static.wixstatic.com/media/8b0c12_cecf94e390e84e5883f4e7c74fca1ca2~mv2.jpg/v1/fill/w_740,h_555,al_c,q_90/8b0c12_cecf94e390e84e5883f4e7c74fca1ca2~mv2.webp)

Tool to set MAC on Controller (Sixaxis Pair Tool) -
https://drive.google.com/file/d/1REzlr4g94jwLLZkhJq9sQmbUKnL9tOMD/view?usp=sharing

To help you find the type of info you can pull from the controller look here - 
[Site](https://github.com/jvpernis/esp32-ps3/blob/master/src/ps3_parser.c)


## Libraries

Dual Motor Driver - TB6612FNG_ESP32
https://github.com/pablopeza/TB6612FNG_ESP32

Accelerometer and Gyro - Adafruit_MPU6050
https://github.com/adafruit/Adafruit_MPU6050

PS3 - esp32-ps3
https://github.com/jvpernis/esp32-ps3

## Notes

If you want to add a servo look here - [Servo](https://gitlab.com/plasmarobotics/plasma-microbot/-/blob/main/Servo.md)

## BOM

| BOM to make 5 robots                             |          |        |         |                                               |
| ------------------------------------------------ | -------- | ------ | ------- | --------------------------------------------- |
| Item                                             | Quantity |        |         | Link                                          |
| ESP32-DevKitC WROOM-32D                          | 1        | $25.77 | $25.77  | [shorturl.at/wJZ03](http://shorturl.at/wJZ03) |
| DC 3V-12V TT Dual Shaft Gear Motor               | 5        | $8.99  | $44.95  | [shorturl.at/jwWZ0](http://shorturl.at/jwWZ0) |
| LM2596 Buck Converter (Tune down to 5v output)   | 1        | $10.98 | $10.98  | [shorturl.at/bekA7](http://shorturl.at/bekA7) |
| TB6612FNG Motor Driver                           | 2        | $11.59 | $23.18  | [shorturl.at/cftP1](http://shorturl.at/cftP1) |
| P3 controller                                    | 5        | $10.59 | $52.95  | [shorturl.at/gGU08](http://shorturl.at/gGU08) |
| PCB                                              | 5        |        |         | [JLCPCB](https://oshwlab.com/erik.pope/plasma-robot-kit)                                        |
| 30 Pcs 40 Pin 2.54mm Male and Female Pin Headers | 3        | $5.39  | $16.17  | [shorturl.at/bxDS4](http://shorturl.at/bxDS4) |
| MPU-6050 3 Axis Gyroscope + Accelerometer        | 1        | $12.99 | $12.99  | [shorturl.at/iluL8](http://shorturl.at/iluL8) |
| Switch Self-Locking                              | 1        | $6.88  | $6.88   | [shorturl.at/kvCFY](http://shorturl.at/kvCFY) |
| LED                                              | 1        | $6.19  | $6.19   | [shorturl.at/bpCHU](http://shorturl.at/bpCHU) |
| Resistor 220 ohm                                 | 1        |        |         | Comes with LEDs                               |
| 2 Pole 2.54mm Screw Terminal                     | 1        | $9.99  | $9.99   | [shorturl.at/lwKLU](http://shorturl.at/lwKLU) |
| Battery                                          | 3        | $26.99 | $80.97  | [shorturl.at/lpzOU](http://shorturl.at/lpzOU) |
|                                                  |          | Total  | $291.02 |
